using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using basiccrud.Models;
using basiccrud.Helpers;

namespace basiccrud.Services
{
    public interface IUserService
    {
        User Authenticate(string email, string password);
        IEnumerable<User> GetAll();
        User Add(string username, string email, string passEnc);
    }

    public class UserService : IUserService
    {        
        private readonly AppSettings _appSettings;
        private readonly CMSContext _context;

        public UserService(IOptions<AppSettings> appSettings, CMSContext _context)
        {
            this._appSettings = appSettings.Value;
            this._context = _context;
        }

        public User Add(string username, string email, string passEnc){
            User user = new User();
            user.Username = username;
            user.Email = email;
            user.Password = passEnc;
            user.RoleId=1;
            user.CreatedAt = DateTime.Now;
            user.UpdatedAt = DateTime.Now;
            _context.Users.Add(user);
            _context.SaveChanges();
            return user;
        }

        public User Authenticate(string email, string password)
        {
            var user = _context.Users.SingleOrDefault(x => x.Email == email && x.Password == password);

            // return null if user not found
            if (user == null)
                return null;

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[] 
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);
            _context.Users.Update(user); 
            _context.SaveChangesAsync();

            user.Password = null;

            return user;
        }

        public IEnumerable<User> GetAll()
        {
            // return users without passwords
            return _context.Users.Select(x => new User(){ Password = null, Username = x.Username, Token = x.Token, CreatedAt = x.CreatedAt, UpdatedAt = x.UpdatedAt, Id= x.Id, Email = x.Email }).ToList();
        }
    }
}