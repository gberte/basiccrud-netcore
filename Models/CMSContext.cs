using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using basiccrud.Models;

namespace basiccrud.Models
{
    /// <summary>
    /// This class defines all the Database Context and declare resultsets and mapping DBModel classes with their related tables
    /// </summary>
    public class CMSContext : DbContext
    {
        public CMSContext(DbContextOptions<CMSContext> options) :base(options){ }

        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Town> Towns { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("users");

                entity.Property( e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e=> e.Username)
                    .HasColumnName("username")
                    .HasColumnType("varchar(150)");

                entity.Property(e=> e.Email)
                    .HasColumnName("email")
                    .HasColumnType("varchar(150)");
                    
                entity.Property(e=> e.Password)
                    .HasColumnName("pasword")
                    .HasColumnType("varchar(100)");

                entity.Property(e=> e.RoleId)
                    .HasColumnName("role_id")
                    .HasColumnType("int");

                entity.Property(e=> e.Active)
                    .HasColumnName("active")
                    .HasColumnType("int");

                entity.Property(e=> e.Token)
                    .HasColumnName("token")
                    .HasColumnType("nvarchar(1024)");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasColumnType("datetime");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasColumnType("datetime");

                entity.HasOne(f => f.Role)
                    .WithMany(m => m.Users)
                    .HasForeignKey(f => f.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("roles");

                entity.Property( e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e=> e.Name)
                    .HasColumnName("name")
                    .HasColumnType("varchar(150)");

                entity.Property(e=> e.Description)
                    .HasColumnName("description")
                    .HasColumnType("varchar(150)");
                    
                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasColumnType("datetime");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Town>(entity =>
            {
                entity.ToTable("towns");

                entity.Property( e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e=> e.Slug)
                    .HasColumnName("slug")
                    .HasColumnType("varchar(50)");

                entity.Property(e=> e.Name)
                    .HasColumnName("name")
                    .HasColumnType("varchar(150)");

                entity.Property(e=> e.State)
                    .HasColumnName("state")
                    .HasColumnType("varchar(150)");

                entity.Property(e=> e.Author)
                    .HasColumnName("author")
                    .HasColumnType("varchar(150)");

                entity.Property(e=> e.Description)
                    .HasColumnName("description")
                    .HasColumnType("nvarchar(1024)");

                entity.Property(e=> e.Image)
                    .HasColumnName("image")
                    .HasColumnType("varchar(255)");
                    
                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasColumnType("datetime");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasColumnType("datetime");
            });
        }
    }
}