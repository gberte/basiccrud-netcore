using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using basiccrud.Models.Persistence;

namespace basiccrud.Models
{
    public class Town : DBModel
    {
        public string Slug { get; set; }
        public string Name { get; set; }
        public string State { get; set; }         
        public string Author { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
    }
}