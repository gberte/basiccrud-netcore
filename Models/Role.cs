using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using basiccrud.Models.Persistence;

namespace basiccrud.Models
{
    public class Role : DBModel
    {
        public string Name { get; set; }
        public string Description { get; set; }         
        public List<User> Users { get; set; }      
    }
}