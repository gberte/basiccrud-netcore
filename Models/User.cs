
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using basiccrud.Models.Persistence;
using Microsoft.AspNetCore.Identity;

namespace basiccrud.Models
{
    public class User : DBModel
    {
        public int Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int Active { get;set; }        
        public int RoleId { get; set; }
        public Role Role { get; set; }

        public string Token { get; set;}
    }


}