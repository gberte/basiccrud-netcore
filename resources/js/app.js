import tinymce from 'tinymce/tinymce'
import 'tinymce/themes/silver/theme'

// Plugins
import 'tinymce/plugins/paste/plugin'
import 'tinymce/plugins/link/plugin'
import 'tinymce/plugins/autoresize/plugin'

tinymce.init({
    selector: 'textarea',
    plugins:['image'],
    toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
    content_css: '//www.tinymce.com/css/codepen.min.css',
    images_upload_url: '/cms/towns/upload',
    images_reuse_filename: true    
});