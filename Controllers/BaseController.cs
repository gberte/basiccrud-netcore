﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using basiccrud.Models;

namespace basiccrud.Controllers
{   
    [Produces("application/json")]
    public class BaseController : Controller
    {
        protected readonly CMSContext context;
        protected readonly IConfiguration config;

        public BaseController (CMSContext _context,IConfiguration _config)
        {
            context = _context;
            config = _config;
        }
    }

}