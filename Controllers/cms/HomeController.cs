﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using basiccrud.Models;
using basiccrud.Helpers;
using basiccrud.Services;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;

namespace basiccrud.Controllers.cms
{   
    [Authorize]
    [Route("cms")]
    public class HomeController : BaseController
    {
        public IUserService userService;

        public HomeController (CMSContext _context,IConfiguration _config, IUserService userService) : base(_context,_config) {
            this.userService = userService;
        }

        [AllowAnonymous]
        public IActionResult Index()
        {
            return  View("~/Views/Cms/Login.cshtml");
        }

        [AllowAnonymous]
        [Route("signup")]
        public IActionResult SignUp()
        {
            return  View("~/Views/Cms/Signup.cshtml");
        }
        
        /* [HttpPost]
        [AllowAnonymous]
        [Route("register")]
        public async Task<ActionResult> Register()
        {
            String username = Request.Form["username"];
            String email = Request.Form["email"];
            String pass = Request.Form["pass"];
            String passEnc = new Encryptor(config).Encrypt(pass);
            User user  = userService.Add(username,email,passEnc);
            return await SignInUser(user); 
        }*/

        [HttpPost]      
        [AllowAnonymous]  
        [Route("login")]
        public async Task<ActionResult> Login()
        {
            String email = Request.Form["email"];
            String pass = Request.Form["pass"];
            String passEnc = new Encryptor(config).Encrypt(pass);
            User user  = userService.Authenticate(email,passEnc);
            if(user == null) return BadRequest("no user do");
            return await SignInUser(user);  
        }

        private async Task<ActionResult> SignInUser(User user){
            var claims = new List<Claim>{
                new Claim(ClaimTypes.NameIdentifier, user.Email),
                new Claim(ClaimTypes.Name, user.Username),
                new Claim(ClaimTypes.Email, user.Email)
            };

            var identity = new ClaimsIdentity(claims,CookieAuthenticationDefaults.AuthenticationScheme);
            var principal = new ClaimsPrincipal(identity);

            await HttpContext.SignInAsync(principal);

            return Redirect("/cms/dashboard");   
        }

        [Route("dashboard")]
        public IActionResult Dashboard()
        {
            return  View("~/Views/Cms/Home.cshtml");
        }

        private User GetUser(string email)
        {
            return context.Users.Where(e => e.Email == email).Include(e => e.Role).FirstOrDefault();
        }

        [HttpGet]      
        [Route("logout")]
        public async Task<ActionResult> Logout()
        {   
            await HttpContext.SignOutAsync();
            return View("~/Views/Cms/Login.cshtml");;  
        }

        [Route("error")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
