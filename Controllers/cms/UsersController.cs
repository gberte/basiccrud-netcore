﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using basiccrud.Models;
using basiccrud.Helpers;
using Microsoft.AspNetCore.Authorization;

namespace basiccrud.Controllers.cms
{
    
    [Route("cms/users")]
    public class UsersController : BaseController
    {
        public UsersController (CMSContext _context,IConfiguration _config) : base(_context,_config) {} 

        public IActionResult Index()
        {
            List<User> users = context.Users.ToList();
            ViewData["users"] = users;
            return  View("~/Views/Cms/Users/List.cshtml");
        }

        [HttpGet]
        [Route("{id?}")]
        public IActionResult Get(int id)
        {
            User user = context.Users.Find(id);
            if(user==null) user = new User();
            ViewData["user"] = user;
            ViewData["error"] = "";
            return View("~/Views/Cms/Users/CrearEditar.cshtml");
        }

        [HttpPost]
        public async Task<IActionResult> Store()
        {
            int id = Int32.Parse(Request.Form["id"]);
            User user = context.Users.Find(id);
            bool newObj = (user == null);            
            if(newObj) user = new User();

            string pass1 = Request.Form["pass1"];
            string pass2 = Request.Form["pass2"];

            //Validate
            if((!String.IsNullOrEmpty(pass1) || !String.IsNullOrEmpty(pass2)) && pass1!=pass2 ){
                ViewData["user"] = user;
                ViewData["error"] = "Las contraseñas no coinciden";
                return View("~/Views/Cms/Users/CrearEditar.cshtml");
            }
        
            user.Username = Request.Form["user"];
            user.Email = Request.Form["email"];
            if(pass1!= "" && pass2 != "" && pass1 == pass2 ){
                user.Password = new Encryptor(config).Encrypt(Request.Form["pass1"]);
            }
            user.RoleId = 1;

            if(newObj) {
                user.UpdatedAt= DateTime.Now;
                user.CreatedAt = DateTime.Now;
                context.Users.Add(user);
            }else{
                user.UpdatedAt = DateTime.Now;
                context.Users.Update(user);                
            }
            await context.SaveChangesAsync();
            
            return Redirect("/cms/users");
        }

        [HttpGet]
        [Route("del/{id?}")]
        public IActionResult Remove(int id)
        {
            User user = context.Users.Find(id);
            context.Users.Remove(user);
            context.SaveChangesAsync();
            return Redirect("/cms/users");
        }

    }
}
