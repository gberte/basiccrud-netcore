﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using basiccrud.Models;

namespace basiccrud.Controllers.cms
{
    [Authorize]
    [Route("cms/towns")]
    public class TownsController : BaseController
    {
        public TownsController (CMSContext _context,IConfiguration _config) : base(_context,_config) {} 

        public IActionResult Index()
        {
            List<Town> towns = context.Towns.ToList();
            ViewData["towns"] = towns;
            return  View("~/Views/Cms/Towns/List.cshtml");
        }

        [HttpGet]
        [Route("{id?}")]
        public IActionResult Get(int id)
        {
            Town town = context.Towns.Find(id);
            if(town==null) town = new Town();
            ViewData["town"] = town;
            return View("~/Views/Cms/Towns/CrearEditar.cshtml");
        }

        [HttpPost]
        public async Task<IActionResult> Store()
        {
            int id = Int32.Parse(Request.Form["id"]);
            
            Town town = context.Towns.Find(id);
            bool newObj = (town == null);            

            //Validate
            if(false){
                ViewData["town"] = town;
                ViewData["error"] = "Validation error";
                return View("~/Views/Cms/Users/CrearEditar.cshtml");
            }

            var file  = Request.Form.Files.First();
            var filePath = "wwwroot/uploads/"+file.FileName;
            String image ="";
            if (file.Length > 0)
            {
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
                image="/uploads/"+file.FileName;
            }

            if(newObj) town = new Town();
            town.Slug = Request.Form["slug"];
            town.Name = Request.Form["name"];
            town.Author = Request.Form["author"];
            town.State = Request.Form["state"];
            town.Description = Request.Form["description"];
            town.Image = image;
            if(newObj) {
                town.UpdatedAt= DateTime.Now;
                town.CreatedAt = DateTime.Now;
                context.Towns.Add(town);
            }else{
                town.UpdatedAt = DateTime.Now;
                context.Towns.Update(town);                
            }
            await context.SaveChangesAsync();
            
            return Redirect("/cms/towns");
        }

        [HttpGet]
        [Route("del/{id?}")]
        public IActionResult Remove(int id)
        {
            Town town = context.Towns.Find(id);
            context.Towns.Remove(town);
            context.SaveChangesAsync();
            return Redirect("/cms/towns");
        }

        [HttpPost]
        [Route("upload")]
        public async Task<IActionResult> Upload()
        {
            var file  = Request.Form.Files.First();
            var filePath = "wwwroot/uploads/"+file.FileName;
            if (file.Length > 0)
            {
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
                var response = new UploadResponse();
                response.location="/uploads/"+file.FileName;
                return Json(response);
            }else{
                return Json(null);
            }       
        }

        public class UploadResponse{
            public string location;
        }

    }
}
