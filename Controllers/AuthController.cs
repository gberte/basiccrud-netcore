using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Microsoft.EntityFrameworkCore;
using basiccrud.Models;
using basiccrud.Helpers;

namespace Backend.Controllers.Back
{
    [Authorize]
    [Route("/auth/")]
    public class AuthController : Controller
    {        
        private readonly CMSContext context;
        private readonly IConfiguration config;
        private readonly Encryptor encryptor;

        public AuthController(CMSContext _context,IConfiguration _config)
        {
            context = _context;
            config = _config;
            encryptor = new Encryptor(config);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("login")]
        public IActionResult Login([FromBody] LoginParams lparams)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);// Verifies that the ModelState is valid

            User user = GetUser(lparams.Email); // Gets user from database.
            if (user == null) return Unauthorized(); // If the user does not exist, returns 401 UNAUTHORIZED status.

            // Returns unauthorized if the passwords do not match.
            if (!encryptor.Compare(lparams.Password, user.Password)) return Unauthorized();

            // Generates JWT and authenticates the user.
            return Ok( new
            {
                roles = new int[] {user.RoleId}
            });
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("logout")]
        public IActionResult Logout([FromBody] LoginParams lparams)
        {
            return Ok( new
            {
                success = true
            });
        }

        // Searches for a user in the database comparing with his username.
        private User GetUser(string email)
        {
            return context.Users.Where(e => e.Email == email).Include(e => e.Role).FirstOrDefault();
        }
    }

    public class LoginParams{
        public string Email {get;set;}
        public string Password { get; set;}

    }
}