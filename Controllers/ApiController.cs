﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using basiccrud.Models;

namespace basiccrud.Controllers
{
    [Route("api")]
    public class ApiController : BaseController
    {
        public ApiController (CMSContext _context,IConfiguration _config) : base(_context,_config) {}

        [Route("data")]
        public JsonResult data()
        {
            List<Town> towns = context.Towns.OrderBy(x=> x.CreatedAt).ToList();
            return Json(towns);
        }
    }
}
